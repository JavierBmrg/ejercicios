<?php
# Añado Kint, para visualizar los arrays con mayor facilidad.
include 'kint/Kint.class.php';

$v[0]=array("nombre" => "Juan", "apellido" => "Perez", "edad" => 26);
$v[1]=array("nombre" => "Carlos", "apellido" => "Gomez", "edad" => 30);
$v[2]=array("nombre" => "Ernesto", "apellido" => "Ramirez", "edad" => 22);



# Desgloso el ejercicio, 

# Hacía referencia en mi escrito a que debe pasarse el parametro (key) a sortear en este caso "edad" para añadirlo como parte del index en el condicional 
#  ex: ($v[$outer][$edadParam] < $v[$inner][$edadParam]) ? pass .. y sigue el ciclo.


$sortBy = "edad";

function sortArray($v,$sortBy){
$length = count($v);
for ($outer = 0; $outer < $length; $outer++):
	for ($inner = 0; $inner < $outer; $inner++):
		if($v[$outer][$sortBy] < $v[$inner][$sortBy]):
			$tmp[] = $v[$outer];
			$v[$outer] = $v[$inner];
			$v[$inner] = $tmp;

		endif;				
	endfor;
endfor;
return $tmp;
}
var_dump(sortArray($v,$sortBy));


/* Versión Simplificada. Al ser un array asociativo (internamente), foreach es una buena arma para tratarlos, y en conjunto con alguna funcion nativa para ordenación resulta un poco más fácil y cumplimos con el principio KISS*/

function sortArrayB($v,$sortBy){
	foreach($v as $key=>$value):
		$sort[$key] = $value[$sortBy];	
	endforeach;

	array_multisort($sort, SORT_ASC, $v);
	
	return $v;

}
d(sortArrayB($v,$sortBy));


