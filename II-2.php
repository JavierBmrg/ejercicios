<?php
# Añado Kint, para visualizar los directorios con mayor facilidad.
include 'kint/Kint.class.php';


# En este caso, habia comentado que habia un error de sintaxis con un ')' en el ciclo. 
# La condición debe mantenerse para seguir el ciclo hasta el fin de directorios.
# Se modificó el else a elseif para no tomar en cuenta . & .. a nivel del primer nivel de directorios.
# Al hacer el llamado de la recursividad se concatenó para identificar la ruta relativa en los próximos directorios(linea 17)
# Como analogía a Bashscript (es de uso común para mi), esto se puede lograr con:
# $ ls -R1 files/ > foo.txt && tr '\n' ' ' < foo.txt

$main = "files/";
function readDirs($main){
$dirHandle = opendir($main);
	while($file = readdir($dirHandle)){
		if(is_dir($main . $file) && $file != '.' && $file != '..'){
			$vec[] = readDirs($main . $file."/");
		} 
		else if($file != '.' && $file != '..'){
			$vec[] = $main.$file;
		}
	}
	return $vec;
}

d(readDirs($main));
